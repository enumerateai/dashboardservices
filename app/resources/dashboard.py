from flasgger import swag_from
from flask.json import jsonify
from flask_restful import Resource
from flask_restful.reqparse import Argument
from service import DashboardService
from util import parse_params

data = {
   "videos":[{"video_id":"123456","url":"https://www.aws.com/watch?v=Epb4g6WKcuw"},
              {"video_id":"123456","url":"https://www.aws.com/watch?v=Epb4g6WKcuw"}
              ], 
   "survey_id":"123346",
   "question_number":1,
   "question":"What is your opinion on Samsung S3?",
   "gender_dist":[
      {
         "male":50
      },
      {
         "female":50
      }
   ],
   "age_dist":[
      {
         "18-25":5
      },
      {
         "25-35":5
      },
      {
         "35-45":5
      },
      {
         ">45":10
      }
   ],
   "overall_sentiment":{
      "value":4,
      "polarity":"positive"
   },
   "counts":{
      "screen":10,
      "battery":10
   },
   "confidence":4,
   "positive_aspects":
[
         {
            "name":"battery",
            "value":10,
            "instance":[{"sentence":"something",
            "video_id":"12345",
            "time":"100"},
            {"sentence":"something",
            "video_id":"12345",
            "time":"100"}] 
            
         },
         {
            "name":"screen",
            "value":10,
           "instance":[{"sentence":"something",
            "video_id":"12345",
            "time":"100"},
            {"sentence":"something",
            "video_id":"12345",
            "time":"100"}] 
         },
         {
            "name":"camera",
            "value":10,
           "instance":[{"sentence":"something",
            "video_id":"12345",
            "time":"100"},
            {"sentence":"something",
            "video_id":"12345",
            "time":"100"}] 
         },
         {
            "name":"fingerprint_sensor",
            "value":10,
           "instance":[{"sentence":"something",
            "video_id":"12345",
            "time":"100"},
            {"sentence":"something",
            "video_id":"12345",
            "time":"100"}] 
         }
      
],
   "negative_aspects":
[
         {
            "name":"battery",
            "value":10,
           "instance":[{"sentence":"something",
            "video_id":"12345",
            "time":"100"},
            {"sentence":"something",
            "video_id":"12345",
            "time":"100"}] 
         },
         {
            "name":"screen",
            "value":10,
           "instance":[{"sentence":"something",
            "video_id":"12345",
            "time":"100"},
            {"sentence":"something",
            "video_id":"12345",
            "time":"100"}] 
         },
         {
            "name":"camera",
            "value":10,
           "instance":[{"sentence":"something",
            "video_id":"12345",
            "time":"100"},
            {"sentence":"something",
            "video_id":"12345",
            "time":"100"}] 
         },
         {
            "name":"fingerprint_sensor",
            "value":10,
           "instance":[{"sentence":"something",
            "video_id":"12345",
            "time":"100"},
            {"sentence":"something",
            "video_id":"12345",
            "time":"100"}] 
         }
      ]
   ,
   "positive_opinions":[
      {
         "opinion":"something1",
         "video_id":"12345",
         "time":"100"
      },
      {
         "opinion":"something2",
         "video_id":"12345",
         "time":"100"
      }
   ],
   "negative_opinions":[
      {
         "opinion":"something1",
         "video_id":"12345",
         "time":"100"
      },
      {
         "opinion":"something2",
         "video_id":"12345",
         "time":"100"
      }
   ]
}

class DashboardResource(Resource):
    """ Verbs relative to the users """
    @staticmethod
    @parse_params(
        Argument('surveyId', required=True, help='get Dashboard'),

    )
    @swag_from('../swagger/create/GET.yml')
    def get(surveyId):
       service = DashboardService()
       dashboard = service.getDashboard(surveyId)
       return dashboard
