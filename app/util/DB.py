from bson.objectid import ObjectId
from pymongo import MongoClient
import config





class DB:
    
    def __init__(self):
        print('loading client')
        source_uri = config.URI
        print(source_uri)
        self.con = MongoClient(source_uri)
    


    def findSurvey(self,surveyId):

        query = {"_id":ObjectId(surveyId)}
        print(surveyId)
        
        surveys=self.con.survey['survey']
        print(query)

        doc = surveys.find_one(query)
        if doc:
            doc["_id"]=str(doc["_id"])
        
        
        return doc
    


