install:
	docker-compose run --rm server pip install -r requirements-dev.txt --user --upgrade --no-warn-script-location

start:
	docker-compose up server

prod-start:
	docker-compose up ProductonServer

coverage:
	docker-compose run --rm testserver bash -c "python -m pytest --cov-report term --cov-report html:coverage --cov-config setup.cfg --cov=src/ test/"

daemon:
	docker-compose up -d server

test:
	docker-compose run --rm testserver

lint:
	docker-compose run --rm server bash -c "python -m flake8 ./src ./test"

safety:
	docker-compose run --rm server bash -c "python vendor/bin/safety check"


build:
	docker build -t dashboardservice .

k-prod:
	docker kill dashboardservice-prod

r-prod:
	docker rm dashboardservice-prod

dev:
	docker run --name dashboardservice-dev -d --restart=unless-stopped --env-file='dev.env' -p 3008:3000 dashboardservice 

k-dev:
	docker kill dashboardservice-dev

r-dev:
	docker rm dashboardservice-dev

prod:
	docker run -d --name dashboardservice-prod --restart=unless-stopped --env-file='prod.env' -p 3000:3000 dashboardservice 

stagging:
	docker run --name dashboardservice-stagging --restart=unless-stopped --env-file='stagging.env' -p 3008:3000 dashboardservice  

k-stag:
	docker kill dashboardservice-stagging

r-stag:
	docker rm dashboardservice-stagging

